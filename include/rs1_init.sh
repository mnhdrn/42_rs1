#1/bin/sh

#==============================================================================#
#------------------------------------------------------------------------------#
# INIT CUSTOM ISSUE

Ip=$(hostname -I | cut -d ' ' -f1)
Status=$(systemctl status apache2 | grep running)

if [ "$Status" = "" ]
then
	Status="Dead"
else
	Status="Running"
fi

Cyan='\033[0;31m'
NC='\033[0m'

echo "" > /etc/issue
echo "                                                      " >> /etc/issue
echo "                                                      " >> /etc/issue
echo "                                                      " >> /etc/issue
echo "                                                      " >> /etc/issue
echo "                      /███████   /██████    /██       " >> /etc/issue
echo "                     | ██__  ██ /██__  ██ /████       " >> /etc/issue
echo "                     | ██  \\\\ ██| ██  \\\\__/|_  ██     " >> /etc/issue
echo "                     | ███████/|  ██████   | ██       " >> /etc/issue
echo "                     | ██__  ██ \\\\____  ██  | ██      " >> /etc/issue
echo "                     | ██  \\\\ ██ /██  \\\\ ██  | ██     " >> /etc/issue
echo "                     | ██  | ██|  ██████/ /██████     " >> /etc/issue
echo "                     |__/  |__/ \\\\______/ |______/    " >> /etc/issue
echo "                                                      " >> /etc/issue
echo "                                                      " >> /etc/issue
echo "                  Welcome in Roger Skyline 1          " >> /etc/issue
echo "                     > ip address  .::.  ${Ip}          " >> /etc/issue
echo "    	             > Wordpress   .::.  ${Status}      " >> /etc/issue
echo "                                                      " >> /etc/issue
echo "                                                      " >> /etc/issue
echo "                                                      " >> /etc/issue

