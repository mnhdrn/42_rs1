#!/bin/bash

#==============================================================================#
#------------------------------------------------------------------------------#
# Flushing all rules

/usr/sbin/iptables -F
/usr/sbin/iptables -X

#==============================================================================#
#------------------------------------------------------------------------------#
# Setting default filter policy

/usr/sbin/iptables -P INPUT DROP
/usr/sbin/iptables -P OUTPUT DROP
/usr/sbin/iptables -P FORWARD DROP

#==============================================================================#
#------------------------------------------------------------------------------#
# Reject spoofed packets

/usr/sbin/iptables -A INPUT -s 10.0.0.0/8 -j DROP
/usr/sbin/iptables -A INPUT -s 169.254.0.0/16 -j DROP
/usr/sbin/iptables -A INPUT -s 172.16.0.0/12 -j DROP
#/usr/sbin/iptables -A INPUT -i enp0s3 -s 127.0.0.0/8 -j DROP

/usr/sbin/iptables -A INPUT -s 224.0.0.0/4 -j DROP
/usr/sbin/iptables -A INPUT -d 224.0.0.0/4 -j DROP
/usr/sbin/iptables -A INPUT -s 240.0.0.0/5 -j DROP
/usr/sbin/iptables -A INPUT -d 240.0.0.0/5 -j DROP
/usr/sbin/iptables -A INPUT -s 0.0.0.0/8 -j DROP
/usr/sbin/iptables -A INPUT -d 0.0.0.0/8 -j DROP
/usr/sbin/iptables -A INPUT -d 239.255.255.0/24 -j DROP
/usr/sbin/iptables -A INPUT -d 255.255.255.255 -j DROP

#==============================================================================#
#------------------------------------------------------------------------------#
# Drop all invalid packets

/usr/sbin/iptables -A INPUT -m state --state INVALID -j DROP
/usr/sbin/iptables -A OUTPUT -m state --state INVALID -j DROP
/usr/sbin/iptables -A FORWARD -m state --state INVALID -j DROP

#==============================================================================#
#------------------------------------------------------------------------------#
# Drop excessive RST packets to avoid smurf attacks

/usr/sbin/iptables -A INPUT -p tcp -m tcp --tcp-flags RST RST -m limit --limit 2/second --limit-burst 2 -j ACCEPT

#==============================================================================#
#------------------------------------------------------------------------------#
# Attempt to block portscans

/usr/sbin/iptables -A INPUT -m recent --name portscan --rcheck --seconds 86400 -j DROP
/usr/sbin/iptables -A FORWARD -m recent --name portscan --rcheck --seconds 86400 -j DROP

#==============================================================================#
#------------------------------------------------------------------------------#
# Once the day has passed, remove them from the portscan list

/usr/sbin/iptables -A INPUT -m recent --name portscan --remove
/usr/sbin/iptables -A FORWARD -m recent --name portscan --remove

#==============================================================================#
#------------------------------------------------------------------------------#
# These rules add scanners to the portscan list, and log the attempt.

/usr/sbin/iptables -A INPUT -p tcp -m tcp --dport 139 -m recent --name portscan --set -j LOG --log-prefix "Portscan:"
/usr/sbin/iptables -A INPUT -p tcp -m tcp --dport 139 -m recent --name portscan --set -j DROP
/usr/sbin/iptables -A FORWARD -p tcp -m tcp --dport 139 -m recent --name portscan --set -j LOG --log-prefix "Portscan:"
/usr/sbin/iptables -A FORWARD -p tcp -m tcp --dport 139 -m recent --name portscan --set -j DROP

#==============================================================================#
#------------------------------------------------------------------------------#
# Deny ping

/usr/sbin/iptables -A OUTPUT -p icmp -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
/usr/sbin/iptables -A INPUT -p icmp -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
/usr/sbin/iptables -A INPUT -p icmp -m limit --limit 5/s -j ACCEPT

#==============================================================================#
#------------------------------------------------------------------------------#
# Prevent scanning

/usr/sbin/iptables -A INPUT -p tcp --tcp-flags SYN,ACK SYN,ACK -m state --state NEW -j DROP 
/usr/sbin/iptables -A INPUT -p tcp --tcp-flags ALL NONE -j DROP 

/usr/sbin/iptables -A INPUT -p tcp --tcp-flags SYN,FIN SYN,FIN -j DROP 
/usr/sbin/iptables -A INPUT -p tcp --tcp-flags SYN,RST SYN,RST -j DROP 
/usr/sbin/iptables -A INPUT -p tcp --tcp-flags ALL SYN,RST,ACK,FIN,URG -j DROP 

/usr/sbin/iptables -A INPUT -p tcp --tcp-flags FIN,RST FIN,RST -j DROP 
/usr/sbin/iptables -A INPUT -p tcp --tcp-flags ACK,FIN FIN -j DROP 
/usr/sbin/iptables -A INPUT -p tcp --tcp-flags ACK,PSH PSH -j DROP 
/usr/sbin/iptables -A INPUT -p tcp --tcp-flags ACK,URG URG -j DROP

#==============================================================================#
#------------------------------------------------------------------------------#
# Allow incoming ssh only

/usr/sbin/iptables -t filter -A INPUT -p tcp --dport 2222 -j ACCEPT
/usr/sbin/iptables -t filter -A OUTPUT -p tcp --sport 2222 -j ACCEPT

#==============================================================================#
#------------------------------------------------------------------------------#
# Allow HTTPS and HTTP only

/usr/sbin/iptables -t filter -A OUTPUT -p tcp --sport 80 -j ACCEPT
/usr/sbin/iptables -t filter -A OUTPUT -p tcp --sport 443 -j ACCEPT
/usr/sbin/iptables -t filter -A INPUT -p tcp --dport 80 -j ACCEPT
/usr/sbin/iptables -t filter -A INPUT -p tcp --dport 443 -j ACCEPT
/usr/sbin/iptables -t filter -A INPUT -p tcp --dport 8443 -j ACCEPT

#==============================================================================#
#------------------------------------------------------------------------------#
# make sure nothing comes or goes out of this box

/usr/sbin/iptables -A INPUT -p all -j DROP
/usr/sbin/iptables -A FORWARD -p all -j DROP
/usr/sbin/iptables -A OUTPUT -p all -j DROP
